//
//  AppDelegate.swift
//  RomRaider
//
//  Created by Nick Fantaske on 3/9/17.
//  Copyright © 2017 Nick Fantaske. All rights reserved.
//

import Cocoa

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {

    func applicationDidFinishLaunching(_ aNotification: Notification) {
        let task = Process()
        task.launchPath = "/System/Library/CoreServices/Jar Launcher.app/Contents/MacOS/Jar Launcher"
        task.arguments = ["/Applications/RomRaider/RomRaider.jar"]
        task.launch()
        exit(0)
    }

    func applicationWillTerminate(_ aNotification: Notification) {
        // Insert code here to tear down your application
    }

}

